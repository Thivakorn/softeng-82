import { Body, Controller, Get, HttpException, Post,HttpStatus,Param } from '@nestjs/common';
import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { Course } from './course.entity';
import { CoursesService} from './courses.service';
import { CreateCourseDto} from './dto/create-course.dto';
import Review from './review.entity';
import {CreateReviewDto} from './dto/create-review.dto';
import {ObjectId, ObjectID} from 'mongodb';
import { ParseObjectIdPipe } from '../common/pipe';

@Controller('courses')
export class CoursesController {
  constructor(private coursesService: CoursesService){}

  @Get()
  async findAll(): Promise<Course[]> {
    return this.coursesService.findAll();
  }

  @Post()
  async create(@Body() createCourseDto: CreateCourseDto){
      return this.coursesService.create(createCourseDto)
  }

  @Get(':courseId/reviews')
  async findAllReview(@Param('courseId', ParseObjectIdPipe) courseId: ObjectId): Promise<Review[]> {
    return this.coursesService.findAllReviews(courseId);
  }

  @Post(':courseId/reviews')
  async createReview(@Param('courseId') courseId: ObjectId,
                    @Body() createReviewDto: CreateReviewDto){
      createReviewDto.courseId = courseId;
      return this.coursesService.createReview(createReviewDto)
  }
}