import {Injectable} from '@nestjs/common';
import Course from './course.entity';
import {Repository} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {CreateCourseDto} from './dto/create-course.dto';
import {CreateReviewDto} from './dto/create-review.dto';
import Review from './review.entity';
import { ObjectId, ObjectID } from 'mongodb';

@Injectable()
export class CoursesService{
    constructor(
      @InjectRepository(Course) 
      private coursesRepository: Repository<Course>,
      @InjectRepository(Review) 
      private reviewRepository: Repository<Review>,
    ){}

    async findAll(): Promise<Course[]>{
      return this.coursesRepository.find();
    }

    async create(createCourseDto: CreateCourseDto){
      return this.coursesRepository.save(createCourseDto);

    }
    async findAllReviews(courseId: ObjectId): Promise<Review[]> {
      return this.reviewRepository.find({where:{courseId: courseId }});
    }

    async createReview(createReviewDto: CreateReviewDto){
      return this.reviewRepository.save(createReviewDto);
    }
}